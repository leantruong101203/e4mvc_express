﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E4MVC_VNEXPPRESS.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Title = "Home - Index";
            return View();
        }
        public ActionResult Info()
        {
            ViewBag.Title = "Info";
            return View();
        }
    }

}